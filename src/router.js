import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from './history';

import HomePage from './views/HomePage';
import MyTimeline from './views/MyTimeline';

import LoginPage from './views/LoginPage';

function AppRouter() {
    return(
        <>
            <Router history={history}>
                <Switch>
                    <Route exact path="/" component={HomePage} />
                    <Route exact path="/my-tl" component={MyTimeline} />
                    <Route exact path="/my-lp" component={LoginPage} />
                </Switch>
            </Router>
        </>
    );
}
export default AppRouter;