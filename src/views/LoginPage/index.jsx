import React, { Component } from 'react';
import './LoginPage.css';
import Vector2 from '../../assets/image/vectorlogin.svg'
import { Grid } from '@material-ui/core';
import Login from '../Component/FormLogin';
import Drawer from '../Component/Drawer';

export default function CenteredGrid() {
  
    return (
        <>
        <Drawer />
        <div className="Container" maxWidth="lg">
            <Grid className="GridKiri" item xs={6}>
              <Login />
            </Grid>
            <Grid className="GridKanan" item xs={6}>
              <img className="VectorLogin" src={Vector2} alt="dummy"/>
            </Grid>
        </div>
     </>
      );
}