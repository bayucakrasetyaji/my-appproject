import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import axios from 'axios'
import VectorHomePage from '../../assets/image/vectorhome.svg';
import PersonList from '../PersonListGet/';
import BasicTable from '../Tabel CRUD/Tabel';
import BasicTextFields from '../Tabel CRUD/Formulir';
import TextOpening from '../Component/ContentHomePage';
import Drawer from '../Component/Drawer';
import Header from '../Component/Header';
import './HomePageStyle.css';


class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      makanan: [],
      name: '',
      calories: 0,
      fat: 0,
      id: '',
    }
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    //memasukan data input kedalam array makanan
    this.setState({
      makanan: [
        ...this.state.makanan,
        {
          id: this.state.makanan.length + 1,
          name: this.state.name,
          calories: this.state.calories,
          fat: this.state.fat,
        },
        ],
    });
    axios.post('https://jsonplaceholder.typicode.com/users', this.state)
    .then(res => {
        console.log(res)
    })
     //membuat kolom inputfield menjadi kosong kembali
    //setelah di submit
    this.setState({
      name: '',
      calories: 0,
      fat: 0,
      id: '',
    });
  };

  render() {

    return (
      <>

      <div className="SectionContent1" maxWidth="lg">
        <div className="LogoMenu">
          <Drawer />
          <Header />
        </div>
          <Grid className="GridContainer" container direction="row" alignItems="center">
            <Grid item lg={6}>
              <TextOpening />
            </Grid>
            <Grid item lg={6}>
              <img src={VectorHomePage} alt="dummy" />
            </Grid>
          </Grid>
      </div>
        <div className="SectionContent2" style={{ height: '100%' }}>
          <Grid item lg={12}>
            <div className="BoxJudul">
              <div className="Judul">GET API</div>
            </div>
            <PersonList />
          </Grid>
        </div>
        <div className="SectionContent3" style={{ height: '100%' }}>
          <h1>Uji Coba Table</h1>
          {/* untuk mengoper semua state yang sudah dibuat*/}
          <BasicTable makanan={this.state.makanan}/>
          <BasicTextFields {...this.state} 
          handleChange={this.handleChange} 
          handleSubmit={this.handleSubmit}/>
        </div>
      </>
    );
  }
}

export default HomePage;
