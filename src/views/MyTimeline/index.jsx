import React, { Component } from 'react';
import './MyTimelineStyle.css';
import Drawer from '../Component/Drawer';
import Header from '../Component/Header';
import Tabel2 from '../Tabel CRUD/Tabel2';

export default class MyTimeline extends Component {
  
  render() {

    return (
      <>
      <div className="Content-Mytl" maxWidth="lg">
          <div className="LogoMenu">
            <Drawer />
            <Header />
        </div>  
        <div className="Text-Content">
          <h1 className="Tittle">My Timeline</h1>
        </div>
            <Tabel2 />
      </div>
      </>
    );
  }
}


// withStyle adalah hook, hook butuh classes
