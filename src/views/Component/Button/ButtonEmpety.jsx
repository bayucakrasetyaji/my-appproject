import React from 'react';


const ButtonEmpety = (props) => {
  return (
    <button className={props.className} onChange={props.handleChange} type={props.type}
    onClick={props.handleChange} >
      { props.children }
    </button>
  );
}

export default ButtonEmpety;