import { IconButton } from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons';
// import EditIcon from '@material-ui/icons/Edit';

// import { makeStyles } from '@material-ui/core/styles'; //modul untuk

// const useStyles = makeStyles((theme)=>({   //untuk memberi style jarak antar button

//       jarakicon: {
//         '& > *': {
//           margin: theme.spacing(0),
//         },
//       },
//   }));

export default function IconButtons() {
  // const classes = useStyles();
  //untuk menggunakan style yg sudah dibuat
  //dengan menambahkan {classes.jarakicon}

  return (
    <div>
      <IconButton>
        <Delete />
      </IconButton>
      <IconButton>
        <Edit />
      </IconButton>
    </div>
  );
}
