import './LoginStyle.css';
import { TextField, Button } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';



const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        display: 'flex',
        flexWrap: 'wrap',
        width: '35ch',
    },
    },
  }));


export default function Login () {
    const classes = useStyles();
    return(
            <div className="formlogin">
                <span>
                    Welcome back <br/>
                  </span>
                  <span className="Text-SubTittle">
                      Login to your account
                  </span> <br/>
                  <div className="Container-FormLogin">
                  <span className={classes.root}>
                      <TextField
                      label="E-mail"
                      placeholder="input your email"
                      InputLabelProps={{shrink: true,}}
                      />
                  </span> <br/>
                  <span className={classes.root}>
                      <TextField
                      type="password"
                      label="Password"
                      placeholder="input your"
                      InputLabelProps={{shrink: true,}}
                      />
                  </span> <br/>
                  <span>
                    <NavLink className="LoginPage" to="/">
                    <Button className="Button-Login" 
                    variant="contained" 
                    color="primary" >Login</Button>
                    </NavLink>
                  </span>
                  </div>
            </div>
    );
}