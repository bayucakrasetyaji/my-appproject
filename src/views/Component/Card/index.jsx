import './CardStyle.css';

export default function Card1({ DataAPI, title }) {
  return (
    <div className="Card-1">
      <h1>{title || 'This title'}</h1>
      {DataAPI}
    </div>
  );
}

//DataAPI,title sebagai Proops
