import React, { Component } from 'react';
import { Table, 
    TableBody, 
    TableCell, 
    TableContainer, 
    TableHead, 
    TableRow, 
    Paper, 
    withStyles,
    makeStyles } from '@material-ui/core';
import axios from 'axios'


    const useStyles = makeStyles((theme) => ({
        table: {
          width: 'fit-content'
        },
      }));

class Tabel2 extends Component {
 
  //constructor wajib dipanggil karena ada extends, agar parent compt tahu children comptnya,
  //dan lebih mudah ketika error atau untuk mengetahui linenya    
    constructor(props) {
        super(props);
    
        this.state = {
          persons: [],
        };
      }
    
      componentDidMount() {
        axios.get('https://jsonplaceholder.typicode.com/users')
        .then((res) => {
          const persons = res.data;
          this.setState({ persons });
          console.log(res);
        });
      }
    
      deleteRow(id, e){
        axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
          .then(res => {
            console.log(res);
            console.log(res.data);
      
            const persons = this.state.persons.filter(item => item.id !== id);
            this.setState({ persons });
          })
      
      }

    render() {
    //mengambil state person yang sudah di deklrasiin
    //classes untuk style, karena memakai hook dia menjadi props
        const {persons} = this.state;
        const {classes} = this.props;

        return (
            <>
             <div style={{paddingBottom: '1rem', width: '80%', margin: 'auto'}}>
            <TableContainer classes="TableContainer" component={Paper}>
              {/*kalau classes biasanya pake root, lalu di style dikasih !important*/}
              <Table className={classes.table} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell className="Header_No">No</TableCell> {/*beri classname untuk mengedit style di css*/}
                    <TableCell align="left">Name</TableCell>
                    <TableCell align="left">Username</TableCell>
                    <TableCell align="left">Email</TableCell>
                    <TableCell align="left">City</TableCell>
                    <TableCell align="left">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {persons.map((row, index) => (
                    <TableRow key={index}>
                      <TableCell component="th" scope="row">
                        {index+1}
                      </TableCell>
                      <TableCell align="left">{row.name}</TableCell>
                      <TableCell align="left">{row.username}</TableCell>
                      <TableCell align="left">{row.email}</TableCell>
                      <TableCell align="left">{row.address.city}</TableCell>
                      <TableCell align="left">{row.action}
                      <button className="btn btn-danger" onClick={(e) => this.deleteRow(row.id, e)}>Delete</button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>   
            </>
        );
    }
}

export default withStyles(useStyles, {withTheme: true})(Tabel2)
// withStyle adalah hook, hook butuh classes