import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';
import ButtonEmpety from '../Component/Button/ButtonEmpety';
import './tabelcrud.css'

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function BasicTextFields({ name, calories, fat, handleChange, handleSubmit }) {
  const classes = useStyles();
  
  return (
    <div className="Formulir">
      <h2>Input Data</h2>
      <form onSubmit={handleSubmit} className={classes.root} noValidate autoComplete="off">
        <TextField 
        id="name" 
        label="Nama Makanan" 
        name="name" 
        value={name} 
        onChange={(event) => handleChange(event)} />
        <br />
        <TextField
          id="calories"
          label="Jumlah Kalori"
          name="calories"
          value={calories}
          onChange={(event) => handleChange(event)}
        />
        <br />
        <TextField 
        id="fat" 
        label="Jumlah Fat" 
        name="fat" value={fat} 
        onChange={(event) => handleChange(event)} />
        <br />
        <ButtonEmpety className="btn-submit" type="Submit"> Submit </ButtonEmpety>
      </form>
    </div>
  );
}
