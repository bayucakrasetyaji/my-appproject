import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  TableHead, 
  TableRow, 
  TableContainer, 
  TableCell, 
  TableBody, 
  Table,
  Paper } from '@material-ui/core';
import ButtonEmpety from '../Component/Button/ButtonEmpety';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});
 

export default function BasicTable({makanan}) {
  const classes = useStyles();

  return (    
    <>
    <TableContainer component={Paper}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell align="center">No</TableCell>
            <TableCell align="center">Nama Makanan</TableCell>
            <TableCell align="center">Calories</TableCell>
            <TableCell align="center">Fat(g)</TableCell>
            <TableCell align="center">Aksi</TableCell> 
          </TableRow>
        </TableHead>
        <TableBody>
          {makanan.map((row, index) => {
            return (
              <>
              <TableRow key={index}>
              <TableCell align="center">{index+1} </TableCell>
              <TableCell align="center">{row.name} </TableCell>
              <TableCell align="center">{row.calories} </TableCell>
              <TableCell align="center">{row.fat} </TableCell>
              <TableCell align="center">
              <button className="btn btn-danger">hapus</button>
              </TableCell>
              </TableRow>
              </>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
    <br/>
    <br/>
    <br/>
    <br/>
    </>

  );
}