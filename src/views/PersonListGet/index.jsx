import React, { Component } from 'react';
import axios from 'axios';
import './PersonGet.css';
import Card1 from '../Component/Card';


export default class PersonList extends Component {
  state = {
    persons: [],
  };

  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/users')
    .then((res) => {
      const persons = res.data;
      this.setState({ persons });
    });
  }

  render() {
    
    const { persons } = this.state;
    const city = [],
          email = [],
          username = [];

    persons.map((person) => {
      city.push(<li className="li">{person.address.city}</li>);
      email.push(<li className="li">{person.email}</li>);
      username.push(<li className="li">{person.username}</li>);
    }); //map untuk iterate/looping sebuah array, push untuk memasukan

    return (
      <div className="RowContent">
        <Card1 DataAPI={username} title="Username" />
        <Card1 DataAPI={email} title="Email" />
        <Card1 DataAPI={city} title="City" />
      </div>
    );
  }
}
